// 标准时间转化
export const FormatDateTime = (timeStamp, status) => {
  var date = new Date();
  date.setTime(timeStamp * 1000);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  var second = date.getSeconds();
  minute = minute < 10 ? ('0' + minute) : minute;
  if (status) {
    // 年月日
    return `${y}/${m}/${d}`;
  } else {
    // 年月日时分秒
    return `${y}-${m}-${d}`;
  }
}
// 包含时间秒
export const FormatDateTimes = (timeStamp) => {
  var date = new Date();
  date.setTime(timeStamp * 1000);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  var second = date.getSeconds();
  var s = second < 10 ? ('0' + second) : second;
  minute = minute < 10 ? ('0' + minute) : minute;
  return `${y}-${m}-${d} ${h}:${minute}:${s}`;
}
// 转时间戳
export const FormatDate = (timeStamp) => {
  var date = new Date(timeStamp)
  return date.getTime() / 1000
}

// 转月份
export const getMonth = (timeStamp) => {
  var date = new Date(timeStamp * 1000)
  return date.getMonth() + 1
}
export function funDate(timeInterval) {
  var currDate = new Date()
  var beforeOneWeekDate = new Date(currDate)
  beforeOneWeekDate.setDate(currDate.getDate() + timeInterval)
  let year2 = beforeOneWeekDate.getFullYear()
  let month2 = beforeOneWeekDate.getMonth() + 1
  let day2 = beforeOneWeekDate.getDate()
  if (parseInt(month2) < 10) {
    month2 = '0' + month2
  }
  if (parseInt(day2) < 10) {
    day2 = '0' + day2
  }
  var formatBeforeOneWeekDate = year2 + '-' + month2 + '-' + day2
  return formatBeforeOneWeekDate
}

// 获取链接上的参数
export const getRequest = () => {
  let url = window.location.search; //获取url中"?"符后的字串
  let theRequest = new Object();
  if (url.indexOf("?") != -1) {
    let str = url.substr(1);
    let strs = str.split("&");
    for (let i = 0; i < strs.length; i++) {
      theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
    }
  }
  return theRequest;
}

//获取当年一年的时间戳
export const yearFormatDate = () => {
  let myDate = new Date();
  let tYear = myDate.getFullYear();
  let isLeap = tYear % 400 == 0 || tYear % 100 != 0 && tYear % 4 == 0;
  let date = 0
  if (isLeap) {
    date = 366
  } else {
    date = 365
  }
  return 60 * 60 * 24 * date * 2 - 15
}

// 获取当前时间后n个月的时间
export const getMonthDay = (curDay,day) => {
  var nowDate = new Date((new Date(curDay*1000)))
  var nowDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
  var nextDate = new Date(nowDate.getFullYear(), nowDate.getMonth() + +day, nowDate.getDate());
  return + ((nextDate.getTime() - nowDate.getTime()) / (1000 * 60 * 60 * 24));
}

// 正式环境
export const apiUrl = "api.php"
// 本地测试
// export const apiUrl = "https://test.laobanyoudan.cn/api_vue.php"