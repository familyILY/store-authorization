import axios from "axios";
// 创建 axios 实例
const service = axios.create({
  baseURL: "",
  timeout: 6000 // 请求超时时间
});

// request interceptor
service.interceptors.request.use(
  config => {
    return config;
  },
  err => {}
);

// response interceptor
service.interceptors.response.use(
  response => {
    return response;
  },
  err => {}
);
export default service;
