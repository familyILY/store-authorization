import Vue from 'vue'
import App from './index_new.vue'
import { Popup,Toast,Loading } from 'vant';
import "vant/lib/Popup/style"
import "vant/lib/Toast/style"
import "vant/lib/Loading/style"
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)
Vue.use(Popup);
Vue.use(Toast);
Vue.use(Loading);
Vue.config.devtools = true;
new Vue({
  el: '#app',
  render: h => h(App)
})