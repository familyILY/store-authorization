import Vue from 'vue'
import App from './index.vue'
import { Checkbox, CheckboxGroup } from 'vant';
import "vant/lib/Checkbox/style"
import "vant/lib/Checkbox-Group/style"
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
new Vue({
  el: '#app',
  render: h => h(App)
})