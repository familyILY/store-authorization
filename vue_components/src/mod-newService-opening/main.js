import Vue from 'vue'
import App from './index_new.vue'
import './font/font.css';
// document.documentElement.style.fontSize = (document.documentElement.clientWidth / 7.5) + 'px'
Vue.config.devtools = true;
new Vue({
  el: '#app',
  render: h => h(App)
})