import Vue from 'vue'
import App from './index.vue'
import { Field,Toast } from 'vant';
import "vant/lib/Field/style"
import "vant/lib/Toast/style"
Vue.config.devtools = true;
Vue.use(Field);
Vue.use(Toast);
new Vue({
  el: '#app',
  render: h => h(App)
})