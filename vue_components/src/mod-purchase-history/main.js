import Vue from 'vue'
import App from './index.vue'
import { List } from 'vant';
import "vant/lib/List/style"
Vue.use(List);

Vue.config.devtools = true;
new Vue({
  el: '#app',
  render: h => h(App)
})