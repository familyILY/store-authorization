import Vue from 'vue'
import App from './index.vue'
import { Toast,Loading } from 'vant';
import "vant/lib/Toast/style"
import "vant/lib/Loading/style"
Vue.use(Toast);
Vue.use(Loading);
Vue.config.devtools = true;

new Vue({
  el: '#app',
  render: h => h(App)
})