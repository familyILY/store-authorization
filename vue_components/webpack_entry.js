const _entry = [
    {
        modName: 'mod-apply-refund',
    },
    {
        modName: 'mod-collection-information',
    },
    {
        modName: 'mod-newService-opening'
    },
    {
        modName: 'mod-notification-center'
    },
    {
        modName: 'mod-opening-record'
    },
    {
        modName: 'mod-order-details'
    },
    {
        modName: 'mod-printer-setting'
    },
    {
        modName: 'mod-purchase-history'
    },
    {
        modName: 'mod-refund-details'
    },
    {
        modName: 'mod-service-opening'
    },
    {
        modName: 'mod-shop-list'
    },
]
module.exports = _entry