
# /vue_components* 说明文档
  + vue_dist 文件夹为打包下的文件
  + src 文件夹为组件目录

  # 打包
  ## webpack方式
    webpack --modules 模块名
    例如： webpack --modules mod-order-details
  ## yarn方式
    yarn build --modules=模块名
    例如：yarn build --modules=mod-order-details
    打包全部模块
    yarn build --modules=mod-all
  
  ## 使用方法
    所需的页面中引入打包后的vue_dist文件夹下对应模块的js与css(相对路径，或者加http://的决定路径)
    例如：
      <link rel="stylesheet" href="./../vue_components/vue_dist/mod-order-details/css/main.css" />
      <script src="./../vue_components/vue_dist/mod-order-details/main.js"></script>
  
  













