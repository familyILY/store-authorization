'use strict'
const webpack = require('webpack')
const path = require('path')
const os = require('os')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const VueLoaderPlugin = require('vue-loader/lib/plugin')
var needHost = '' // 打开的host
try {
  let network = os.networkInterfaces() // 获得网络接口列表。
  needHost = network[Object.keys(network)[0]][1].address // 本机ip
  if (network.en0) {
    needHost = (network.en0[1].address)
  }
} catch (e) {
  needHost = 'localhost'
}
module.exports = (env, argv) => {
  console.log('正在打包')
  if (!argv['modules']) {
    console.log('[严重警告]错误 => 入口模块未定义!')
    console.log('[严重警告]请查找该文件是否存在==>', argv['modules'])
    process.exit()
  }
  // 入口目录
  const entryPath = `./src`
  // 打包输出目录
  const buildOuterPath = `${entryPath}/${argv['modules']}/dist`
  const devWebpackConfig = {
    mode: 'development',
    entry: `${entryPath}/${argv['modules']}/main.js`,
    output: {
      filename: `./dist/main.js`
    },
    devtool: 'inline-source-map',
    devServer: {
      host: needHost,
      port: 8828,
      historyApiFallback: true,
      hot: true,
      contentBase: `${entryPath}/${argv['modules']}/dist`
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: `${entryPath}/${argv['modules']}/dist/index.html`
      }),
      // css 输出
      new ExtractTextPlugin(`./dist/main.css`),
      new webpack.HotModuleReplacementPlugin(),
      new VueLoaderPlugin()
    ],
    // 路径
    resolve: {
      alias: {
        "@src": path.join(__dirname, "./src"),
        "@util": path.join(__dirname, "./src/utils"),
        "@com": path.join(__dirname, "./src/components"),
      },
      extensions: [".js", ".vue", ".css", ".less"]
    },
    module: {
      rules: [
        // babel-loader @7.x
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components|static)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              // 按需引入vant 
              plugins: [
                [
                  "import",
                  {
                    libraryName: "vant",
                    libraryDirectory: "es",
                    style: true
                  },
                  "vant"
                ]
              ]
            }
          }
        },
        // vue-loader
        {
          test: /\.vue$/,
          use: 'vue-loader'
        },
        // css-loader
        {
          test: /\.(less|css)$/,
          // exclude: /node_modules/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [{
                loader: "css-loader"
              },
              {
                loader: "less-loader"
              }
            ],
          })
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [{
                loader: "css-loader"
              },
              {
                loader: "sass-loader"
              }
            ],
          })
        },
        // 图片loader
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: '[name].[ext]',
            outputPath: `./dist/img/`,
            publicPath: url => './img/' + url
          }
        },
        // 媒体loader
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: buildOuterPath + '/media/[name].[ext]'
          }
        },
        // 字库loader
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: '[name].[ext]',
            outputPath: `${buildOuterPath}/font/`,
            publicPath: url => './font/' + url
          }
        }
      ]
    },
  }
  return devWebpackConfig
}